import React, { Component } from "react";

import Cell from './Cell';

export class Board extends Component {
  render() {
    console.log(this.props.board_list);

    return (
        <div className="board">
            {this.props.board_list.map(list => { 
                return <Cell key={list.id} symbol={list.symbol} />
            })}
        </div>
    );
  }
}

export default Board;
