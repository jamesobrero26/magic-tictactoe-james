import React, { Component } from "react";
import { connect } from "react-redux";
import { setSymbol } from "../actions/actions";

export class Cell extends Component {
  render() {
    return (
      <div
        className="cell"
        onClick={e => {
          this.props.setSymbol(this.props.board_key);
        }}
      >
        {this.props.symbol}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  ...state
});

const mapsDispatchToProps = dispatch => ({
  setSymbol: boardIndex => dispatch(setSymbol(boardIndex))
});

export default connect(
  mapStateToProps,
  mapsDispatchToProps
)(Cell);
