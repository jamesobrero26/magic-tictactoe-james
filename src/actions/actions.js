export const setSymbol = (board_index) => dispatch => {
  dispatch({
    type: "SET_SYMBOL",
    board_index
  });
};
