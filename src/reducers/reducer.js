const initialState = {
  board: [
    "", "", "",
    "", "", "",
    "", "", ""
  ],
  player1: "",
  player2: "",
  turn: "X",
  winer: "n"
}

export default (state = initialState, action) => {
  switch (action.type) {
    case "SET_SYMBOL":
      const newBoard = state.board;
      newBoard[action.board_index] = state.turn;

      const newTurn = state.turn === "X" ? "0" : "X";

      return {
        ...state,
        board: newBoard,
        turn: newTurn
      }
    default:
      return state;
  }
};
