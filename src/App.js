import React, { Component } from "react";
import { connect } from "react-redux";

import Cell from "./components/Cell";

import "./App.css";
import { dispatch } from "rxjs/internal/observable/range";

export class App extends Component {

  render() {
    return (
      <div className="container">
        <h1 className="title">Tictatoe</h1>
        <div className="board">
          {this.props.reducer.board.map((symbol, index) => {
            return (
              <Cell
                key={index}
                symbol={symbol}
                board_key={index}
              />
            );
          })}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  ...state,
})

const mapsDispatchToProps = dispatch => ({

})

export default connect(mapStateToProps, mapsDispatchToProps)(App);
